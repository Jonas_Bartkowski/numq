#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>



void adv(int *ba, int *bx, int **pta)
{
    if (*pta+1 == bx-1)
        *pta = ba;
    else
        *pta = *pta+1;
}

int numq(int *ba, int *bx, int **ca, int **cx, int inp, int subs)
{
    //if subs is set to true
    if (subs != 0)
    {
        //if queue is not empty
        if (*cx != bx)
        {
            //move cx one step forward (aka dequeue an entry) - with wrap around
            adv(ba, bx, cx);
        }
        else
            return -1;
    }

    //if queue is full, abort, don't do anything
    if (*ca == *cx)
        return -1;

    //enque input
    **ca = inp;

    //set oldest-entry pointer to first element if queue was empty before
    if (*cx == bx)
        *cx = ba;

    //advance *ca with wrap-around
    adv(ba, bx, ca);

    int ret;
    //if queue is not full or empty
    if (*cx != *ca)
    {
        //return slots between *cx & *ca (aka number of empty slots)
        int slots = bx - 1 - ba;
        int dif = (*ca - *cx);

        if (dif > 0)
            ret = slots - dif;
        else
            ret = slots - (slots - dif*-1);
    }
    else
        ret = 0;

    return ret;
}


void test()
{
    printf("starting test\n");

    int x = 5;
    int *xp = &x;
    int **xpp = &xp;

    printf("vals: %d %d %d\n", x, *xp, **xpp);

    int queue[8];
    int *start = queue;
    int **np = &start;

    *start = 0;
    printf("queue[0] = %d\n", queue[0]);
    **np = 69;
    *np = start + 1;
    **np = 420;

    printf("vals: %d %d %d\n", *start, queue[0], queue[1]);
}

void p_buf(int *ba, int *bx)
{
    //p_pts("st b_buf cax:");
    for (int *i = ba; i < bx-1; i += 1)
    {
        printf("%d: %d\n", (i - ba), *i);
    }
    //p_pts("ed p_buf cax:");
}

int main()
{
    //int *queue = malloc(6 * sizeof(int));
    int queue[6];
    int *ba = queue;
    int *bx = queue+6;

    int *qp = queue;
    int **ca = &(qp);
    int *qep = (queue+6);
    int **cx = &qep;


    //**np = 420;
    int inp;
    scanf("%d", &inp);

    int slots = (sizeof(queue) / sizeof(int)) - 1;
    int empty_slots = slots;
    while(inp != EOF)
    {
        //p_pts("prnumq cax:");

        if (inp != -2)
            empty_slots = numq(ba, bx, ca, cx, inp, 0);
        else
            empty_slots = numq(ba, bx, ca, cx, inp, 1);

        scanf("%d", &inp);
    }


    int numPrint = 4;
    if (slots-empty_slots < 4)
        numPrint = slots-empty_slots;

    long slots_after = (*ca-*cx) + (*cx - ba);
    long slots_before = numPrint - slots_after;

    //printf("np = %i, es = %i, sa = %i, sb = %i\n", numPrint, empty_slots, slots_after, slots_before);

    for (int *i = bx-1-slots_before; i < bx-1; i++)
    {
      printf("%d\n", *i);
    }

    for (int *i = ba; i < ba+slots_after; i++)
    {
      printf("%d\n", *i);
    }

    return 0;
}
